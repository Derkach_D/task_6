<?php

$secret = rand(); 
$_SESSION['secret'] = $secret;
$salt = rand();
$token = $salt.":".md5($salt.":".$secret); 

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.


// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<form action="" method="post">
  <input type="hidden" name="csrf" value="<?php $token ?>">
  <input name="login" />
  <input name="pass" />
  <input type="submit" value="Войти" />
</form>

<?php

}

// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  $user = 'u20341';
  $pass = '3227715';
  $db = new PDO('mysql:host=localhost;dbname=u20341', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        try{
          $l= $_POST['login'];
          $stmt = $db->prepare("SELECT * FROM task_5 WHERE LOGIN = ?");
          $stmt->execute(array($l));
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          setcookie('epssw', $row['PASSWORD']);
          if ($row['PASSWORD']!=$_POST['pass']){  
            setcookie('wrong', 1);    //если пароль из бд не совпадает с введенным, то перенаправляем  
             header('Location: messages.php');   //на страницу с сообщением неверные данные входа
          }
          setcookie('elogin', $l);
          // Выдать сообщение об ошибках.

          // Если все ок, то авторизуем пользователя.
          $_SESSION['login'] = $_POST['login'];
          // Находим id пользователя по логину
          $_SESSION['uid'] = $row['ID'];
          setcookie('uid', $row['ID']);
          //ВСТАВЛЯЕМ В ФОРМУ ЗНАЧЕНИЯ, РАНЕЕ ВВЕДЕННЫЕ ПОЛЬЗОВАТЕЛЕМ
          //$_POST['fio']=$row['name'];
        }
     
        catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
    
  // Делаем перенаправление.
  header('Location: form.php');
}
