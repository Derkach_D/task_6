<?php

include 'configDB.php';
/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
$flag = true;
if (!empty($_GET['logout'])){
  $request_path=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
  header("Location: http://x:x@".$_SERVER['SERVER_NAME'].$request_path);
  exit();
}
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']))
     {
       $flag = false;
}
else {
$sh = $db->prepare("SELECT * FROM admins WHERE hash_pass = ? AND login = ?");
$sh->execute(array(md5($_SERVER['PHP_AUTH_PW']),$_SERVER['PHP_AUTH_USER']));
if (!$sh->fetchColumn()){
  $flag=false;
}
}
if (!$flag){
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}

if (!empty($_GET['delete'])){
  $stmt = $db->prepare("DELETE FROM task_5 WHERE ID = ?");
  $stmt->execute(array($_GET['delete']));
  print("Успешно удалено!");
}
//print('Вы успешно авторизовались и видите защищенные паролем данные.');

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********

//$l=1;
$stmt = $db->query("SELECT * FROM task_5");
//$row = $stmt;//->fetch(PDO::FETCH_ASSOC);
//var_dump($stmt); //$row['id']['vr'];
?>
<html>
  <head>
  <style>
 body{
   background-color:brown; 
 }
 table{
     background-color: rgb(236, 182, 101);
     border-style:solid;
 }
 td{
    border-style:solid;
    border-color: white;
 }
 h1{
     font-size:30px;
     margin-left:30%;
     text-shadow: 1px 1px 1px white;
     
 }
 </style>
 <?php
    
 ?>
  </head>
  <body>
  <h1>Вы вошли как администратор</h1>
  <form method="GET">
  <input type="hidden" value="1" name="logout">
  <input type="submit" value="Выйти">
  </form>
    <table>
      <tr>
        <td>ID</td>
        <td>name</td>
        <td>agreement</td>
        <td>LOGIN</td>
        <td>PASSWORD</td>
        <td>email</td>
        <td>birthday</td>
        <td>gender</td>
        <td>legs</td>
        <td>deathless</td>
        <td>idclip</td>
        <td>fly</td>
        <td>biography</td>
        <td>Действие</td>
      </tr>
      <tr>
      <?php 
        foreach($stmt as $row){
          ?><tr>
                <td><?php echo $row['ID'] ?></td>
                <td><?php echo $row['name']?></td>
                <td><?php echo $row['agreement'] ?></td>
                <td><?php echo $row['LOGIN'] ?></td>
                <td><?php echo $row['PASSWORD'] ?></td>
                <td><?php echo $row['email'] ?></td>
                <td><?php echo $row['birthday'] ?></td>
                <td><?php echo $row['gender'] ?></td>
                <td><?php echo $row['legs'] ?></td>
                <td><?php echo $row['deathless'] ?></td>
                <td><?php echo $row['idclip'] ?></td>
                <td><?php echo $row['fly'] ?></td>
                <td><?php echo $row['biography'] ?></td>
                <td><form method="GET"><input type="submit"  value="Удалить данные"></input> 
                <input type="hidden" name="csrf" value="<?php $token ?>">
                <input type="hidden" value=<?php echo $row['ID']?> name="delete"></form></td>
            </tr><?php
}
        ?>        
      </tr>
    </table>
  </body>
</html>